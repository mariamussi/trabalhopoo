package com.trabalhopoo.casa.entity;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
//argumentos que facilitariam o trabalho através do framework
// @Data
// @AllArgsConstructor
// @NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Evento {

    // anotations = mapeiam a entidade do banco
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String titulo;
    private String data;
    private String path;


    public Evento(){}

    public Evento(
        Long id, String titulo, String data, String path
    ){
        this.id = id;
        this.titulo = titulo;
        this.data = data;
        this.path = path;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

      /**
     * @param data the date to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * @return the date
     */
    public String getData() {
        return data;
    }

      /**
     * @param path the path to set
     */
    public void setPath (String path) {
        this.path= path;
    }

     /**
     * @return the date
     */
    public String getPath() {
        return path;
    }

    @Override
    public String toString() {
        return String.format(
                "Evento[id=%d, titulo='%s']",
                id, titulo);
    }

}