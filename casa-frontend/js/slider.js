$(window).on('load', function() {

  console.log('All assets are loaded')

  var imagens  = $("#imagens").children('li'),
      dot        = $("#dots").children('span'),
      numImagens = imagens.length,
      tempo      = 3500,
      rodar      = setInterval(autoPlay, tempo),
      i            = 0,
      a;

  function imagemSeguinte() {
      imagens.eq(i).removeClass('active');
      dot.eq(i).removeClass('active');
      i = ++i === numImagens ? 0 : i;
      imagens.eq(i).addClass('active');
      dot.eq(i).addClass('active');
  };
  
  function imagemAnterior() {
      imagens.eq(i).removeClass('active');
      dot.eq(i).removeClass('active');
      i = --i === -1 ? numImagens -1 : i;
      imagens.eq(i).addClass('active');
      dot.eq(i).addClass('active');
  };
  
  function mudarImagem( i, a ) {
      clearInterval(rodar);

      // remover
      imagens.eq(a).removeClass('active');
      dot.eq(a).removeClass('active');

      // adicionar
      imagens.eq(i).addClass('active');
      dot.eq(i).addClass('active');
      
      rodar = setInterval(autoPlay, tempo);       
  };
  
  $('#slider .container').hover(function(){
      $('#botoes').fadeIn(300);
      clearInterval(rodar);
  }, function(){
      $('#botoes').fadeOut(300);
      rodar = setInterval(autoPlay, tempo);
  });
  
  $('#anterior, #seguinte').on('click', function (e) {
      e.preventDefault();
      if( this.id === 'seguinte' ) {
          imagemSeguinte();
      } else {
          imagemAnterior();
      }
  });
  
  $('.dot').on('click', function (e) {
      e.preventDefault();

      i = $(this).index();
      a = $('#dots').children('span.active').index();

      mudarImagem( i, a );        
  });
  
  function autoPlay() {
      $('#seguinte').click();
  }

}); 