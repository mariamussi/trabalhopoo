//menu-hamburguer
$('.mobile-btn').click(function(){
  $(this).toggleClass('active')
  $('.menu-mobile').toggleClass('active')
});

//album de fotos
$('#album-left').click(function(){
  $('#album-grandparents').addClass('active-album')
  $('#album-events').removeClass('active-album')
});

$('#album-right').click(function(){
  $('#album-events').addClass('active-album')
  $('#album-grandparents').removeClass('active-album')
});