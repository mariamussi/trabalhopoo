package com.trabalhopoo.casa.controller;

import java.util.List;
import java.util.Optional;

import com.trabalhopoo.casa.entity.Evento;
import com.trabalhopoo.casa.repository.EventoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
//consome o repositorio
public class EventoController{

    @Autowired //gera uma instancia automatica do objeto, instanciando repository automaticamente.
    private EventoRepository repository;

    //metodos que vão responder ao request do repositorio
    @RequestMapping(value = "/evento/listAll", method= RequestMethod.GET)
    public ResponseEntity< Iterable<Evento>> listaAll(){

        Iterable<Evento> lista = this.repository.findAll();
            return ResponseEntity.ok().body(lista);
    }

    //salva um evento. Através do método post você solicita o evento e salva
    @RequestMapping(value = "/evento/save", method= RequestMethod.POST)
    public ResponseEntity< Evento> save(@RequestBody Evento entity){

        return ResponseEntity.ok().body(this.repository.save(entity));
    }

    //busca o evento pelo parametro Id
    @RequestMapping(value = "/evento/{id}", method= RequestMethod.GET)
    public ResponseEntity<Evento> findById(@PathVariable Long id){
        
        Optional<Evento> evento = this.repository.findById(id);
        if (evento.isPresent())
            return ResponseEntity.ok().body(evento.get());
        else
            return ResponseEntity.ok().body(new Evento());
    }
    
    //deleta um evento através do ID fornecido.
    @RequestMapping(value = "/evento/delete", method = RequestMethod.POST)
    public ResponseEntity<String> delete(@RequestParam Long id) {
        
        Optional<Evento> entidade = repository.findById(id);
        //se o id existe, é deletado
        if (entidade.isPresent())
            this.repository.delete(entidade.get());
        return ResponseEntity.ok().body("{}");
    }
    
    //cria uma lista com os 4 primeiros eventos com ID em ordem decrescente, ou seja, pega os 4 ultimos
    @RequestMapping(value = "/evento/listFour", method= RequestMethod.GET)
    public ResponseEntity< Iterable<Evento>> listFour(){

        List<Evento> eventos = this.repository.queryFirst4ByOrderByIdDesc();
            return ResponseEntity.ok().body(eventos);
    }

}