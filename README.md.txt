Trabalho POO - Funda��o CASA

Java Web feito com front em HTML e CSS e back com Spring Boot.

O principal do projeto se encontra no seginte path: trabalhopoo\casa\src\main\java\com\trabalhopoo\casa,
onde est�o as pastas que se encarregam de formar o MVC (Model, view e controller) do projeto.
No path: trabalhopoo\casa-frontend se encontram os arquivos .html onde foi elaborado o front-end e foram feitas
as requisi��es AJAX com Jquery. 

O banco de dados usado foi o Postgres com pgAdmin4. Nao foi poss�vel exportar para a aplica��o.
Esse banco de dados cont�m uma tabela "evento" com as seguintes colunas: "id, path(referente a imagem), descricao e data". 