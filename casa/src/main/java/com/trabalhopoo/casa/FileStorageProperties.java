package com.trabalhopoo.casa;

import org.springframework.boot.context.properties.ConfigurationProperties;

//regras a serem cumpridas para o upload de arquivos
@ConfigurationProperties(prefix = "file")

//definição do diretorio das imagens. 
public class FileStorageProperties {
    private String uploadDir;

    public String getUploadDir() {
        return uploadDir;
    }

    public void setUploadDir(String uploadDir) {
        this.uploadDir = uploadDir;
    }
}