package com.trabalhopoo.casa.repository;

import java.util.List;

import com.trabalhopoo.casa.entity.Evento;

import org.springframework.data.repository.CrudRepository;

//mapeia as funcionalidades de acesso ao banco. O unico lugar onde o banco é acessado
public interface EventoRepository extends CrudRepository<Evento, Long> {

   //acessa o bd e pega os 4 primeiros ids em ordem decrescente
   List<Evento> queryFirst4ByOrderByIdDesc();

}